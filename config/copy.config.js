/**
 * Custom config - copy files to www
 * Redefine propiedades de node_modules/@ionic/app-scripts/config/copy.config.js
 *      para no copiar fonts default (roboto, noto sans)
 * Las fonts usadas en la app (Lato), las copia por la tarea copyAssets (copia todo lo que está en src/assets)
 *
 * https://github.com/driftyco/ionic-app-scripts#custom-configuration
 *      Note that Ionic will always apply its defaults for any property that was not provided by custom configurations.
 */

module.exports = {
  copyFonts: {
    // sólo copia ionicons
    src: [ '{{ROOT}}/node_modules/ionicons/dist/fonts/ionicons.woff', '{{ROOT}}/node_modules/ionicons/dist/fonts/ionicons.woff2' ],
    dest: '{{WWW}}/assets/fonts'
  },
}