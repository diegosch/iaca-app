import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, App, MenuController, ToastController,
         PopoverController, ViewController, IonicApp, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthService } from '../providers/auth.service';
import { ConnectivityService } from '../providers/connectivity.service';

import { TITLE_APP, PAGES  } from './config';

@Component({
  templateUrl: 'app.html'
})
export class IACAApp {
  @ViewChild(Nav) nav: Nav;

  // Página principal (inicio): login o resultados
  get mainPage(): string {
    return this.auth.logueado ? PAGES.resultados : PAGES.login;
  }

  // Variables para manejar boton back
  lastBackButton = Date.now();
  allowClose = false;
  dismissing = false;

  constructor(public platform: Platform, public auth: AuthService, private app: App, private ionicApp: IonicApp,
              private menu: MenuController, private toastCtrl: ToastController, private events: Events,
              private statusBar: StatusBar, private splashScreen: SplashScreen,
              private popoverCtrl: PopoverController, public connectivity: ConnectivityService)
  {
    this.app.setTitle(TITLE_APP);
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#28427E');

      // Chequea si hay usuario en local storage (login en ejecuci) y luego esconde splash
      // Si hay crea el usuario y setea el valor auth.user. Si no queda en null.
      //    En ambos casos retorna promise ok, con boolean indicando si hay login.
      this.auth.checkLoginPrevio().then(logueado => {
        if (logueado) {
          this.nav.setRoot(PAGES.resultados)
        }
        else {
          this.nav.setRoot(PAGES.login)
        }
        this.splashScreen.hide();
      });

      // Subscripcion a eventos lanzados por services o pages, que alteran globalmente el estado de la app
      this.suscripcionEventos();

      // Botón back: cierra menú, o vuelve atrás o pide apretar dos veces para cerrar app
      this.platform.registerBackButtonAction(() => {
        this.handleBackButtonCloseApp();
      });
    });
  }

  private suscripcionEventos() {
    // Listen logout para redirigir a login
    this.events.subscribe('auth:logout',() => {
      // console.log('Logout: redirect login');
      this.nav.setRoot(PAGES.login);
    });
    // Listen cambio usuario para redirigir a login (pero no eliminó usuario previo aún)
    this.events.subscribe('auth:cambio_usuario',() => {
      this.nav.setRoot(PAGES.login);
    });
  }

  openPage(page_name, params = null) {
    this.nav.setRoot(PAGES[page_name], params)
      .then(ok => {
        if (!ok) {
          this.nav.setRoot(this.mainPage);
        }
      });
  }

  showPopoverPaciente(event) {
    let popover = this.popoverCtrl.create(PopoverPaciente,{},{cssClass:'popover-paciente-sidemenu'});
    popover.present({ev: event});
  }

  /**
   * Método Ejecutado cuando presiona botón back hardware
   *  Cierra menú/popover o vuelve atrás si puede,
   *    si no cierra app pidiendo que presione dos veces (muestra toast)
   */
  private handleBackButtonCloseApp() {

    // Cierra menú
    if (this.menu.isOpen()) {
      this.menu.close();  return;
    }

    // Chequeo si hay una view por fuera del stack de nav (ej: popover, modal).
    //  Uso métodos "hidden" de IonicApp, pero es la única manera que encontré para obtener popover activo
    let activePortal = this.ionicApp._getActivePortal();
    let activeView = (activePortal) ? activePortal.getActive() : this.nav.getActive();
    if (activeView.isOverlay) {
      // Chequeo si no se está cerrando
      if (!this.dismissing) {
        this.dismissing = true;
        activeView.dismiss().then(() => { this.dismissing = false; });
      }
      return;
    }

    // Vuelve atrás si puede
    if (this.nav.canGoBack()) {
      this.nav.pop(); return;
    }
    else if (this.nav.getActive().id == PAGES.sede){
      this.nav.setRoot(PAGES.sedes); return;
    }
    else if (this.nav.getActive().id != this.mainPage){
      this.nav.setRoot(this.mainPage); return;
    }

    // Si llega a este punto,
    // Doble tap en back para cerrar app: debe presiona dos veces dentro de 2 segundos
    //    pero con una separación de 500ms entre cada tap
    // https://forum.ionicframework.com/t/how-to-hold-back-twice-to-exit/80544/4
    // https://stackoverflow.com/questions/41373774/how-to-handle-back-button-on-ionic-2
    const closeDelay = 2000;
    const spamDelay = 500;

    if(Date.now() - this.lastBackButton > spamDelay && !this.allowClose) {
      this.allowClose = true;
      let toast = this.toastCtrl.create({
        message: "Presioná nuevamente para cerrar la aplicación",
        duration: closeDelay,
        dismissOnPageChange: true
      });
      toast.onDidDismiss(() => {
        this.allowClose = false;
      });
      toast.present();
    }
    else if(Date.now() - this.lastBackButton < closeDelay && this.allowClose) {
      this.platform.exitApp();
    }
    this.lastBackButton = Date.now();
  }

}

/**
 * Popover para cambiar usuario y logout
 */
@Component({
  selector: 'popover-paciente',
  template: `
    <span class="paciente-link t5" (click)="irLogin()">Cambiar usuario</span>
    <span class="paciente-link t5" (click)="logout()">Cerrar sesión</span>
    `
})
export class PopoverPaciente {

  constructor(private viewCtrl: ViewController, private auth: AuthService,
              private menu: MenuController, private events: Events) { }

  irLogin() {
    this.viewCtrl.dismiss();
    // Va a login sin desloguear el usuario actual
    //  Si loguea nuevo usuario lo reemplaza.
    //  Si no, puede volver a la pagina de resultados del usuario anterior desde el menú.
    this.events.publish('auth:cambio_usuario');
    this.menu.close();
  }

  logout() {
    this.viewCtrl.dismiss();
    this.auth.logout();
    this.menu.close();
  }
}

