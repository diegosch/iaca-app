// Vendor (angular, ionic)
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';

// Config y componente principal
import { IONIC_CONFIG } from './config';
import { IACAApp, PopoverPaciente } from './app.component';
import { ComponentsModule } from '../components/components.module';

// Config y services (providers)
import { ShiftWSService } from '../providers/shiftWS.service';
import { AuthService } from '../providers/auth.service';
import { ResultadosService } from '../providers/resultados.service';
import { ConnectivityService } from '../providers/connectivity.service';

// Providers Ionic Native
import { HTTP } from '@ionic-native/http';
import { AppAvailability } from '@ionic-native/app-availability';
import { Device } from '@ionic-native/device';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Network } from '@ionic-native/network';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { FileOpener } from '@ionic-native/file-opener';
import { GoogleMaps } from '@ionic-native/google-maps';


@NgModule({
  declarations: [
    IACAApp,
    PopoverPaciente
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(IACAApp, IONIC_CONFIG),
    IonicStorageModule.forRoot({
      name: 'iaca'
    }),
    HttpClientModule,
    ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    IACAApp,
    PopoverPaciente
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ShiftWSService,
    AuthService,
    ResultadosService,
    ConnectivityService,
    HTTP,
    AppAvailability,
    Device,
    InAppBrowser,
    Network,
    SplashScreen,
    StatusBar,
    File,
    FileTransfer,
    FileOpener,
    GoogleMaps
  ]
})
export class AppModule {}
