/**
 * Definición de constantes globales
 */

/* Title app */
export const TITLE_APP = 'IACA Laboratorios';

/* Pages menu */
export const PAGES = {
  login: 'LoginPage',
  resultados: 'ResultadosPage',
  sedes: 'SedesPage',
  sede: 'SedePage',
  informacion: 'InformacionPage',
  contacto: 'ContactoPage'
};

/** URL WebService (protocolo SOAP) **/
// URL del web service
export const WEBSERVICE_URL = "http://181.30.97.58/shift/lis/iaca/elis/s01.util.b2b.integracaoMobile.Webserver.cls";
// URL con proxy para desarrollo (agrega header CORS, definido en ionic.config.json - funciona con ionic serve)
export const WEBSERVICE_URL_PROXY = "/shift-ws";

// Métodos definidos por el webservice
// WDSL: http://181.30.97.58/shift/lis/iaca/elis/s01.util.b2b.integracaoMobile.Webserver.cls?WSDL
export const WS_METHODS = {
  login: 'WsLogin',
  resultados: 'WsListaExPaciente'
};
export const WS_SOAPACTION_URL = 'http://www.shift.com.br/s01.util.b2b.integracaoMobile.Webserver.';

/* Config app ionic (set defaults visuales) - http://ionicframework.com/docs/api/config/Config/ */
export const IONIC_CONFIG = {
  menuType: "overlay",
  backButtonText: '',
  pageTransition: 'ios-transition',
  platforms: {
    ios: {
      statusbarPadding: true,
    }
  }
};

// API keys google maps
export const GOOGLE_MAPS_API_KEY_ANDROID = 'AIzaSyD2ARb7l7gAlrmK3xkpciq9Q7so8qX3Ov0';
export const GOOGLE_MAPS_API_KEY_IOS = 'AIzaSyCftOPjWc7FOU8CARdiRrtgQ6TtadXKymM';