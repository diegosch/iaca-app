import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common"
import { IACAIconComponent } from './iaca-icon/iaca-icon';
import { SedeContactoComponent } from './sede-contacto/sede-contacto';

@NgModule({
	declarations: [
        IACAIconComponent,
        SedeContactoComponent
    ],
	imports: [CommonModule],
	exports: [
        IACAIconComponent,
        SedeContactoComponent
    ]
})
export class ComponentsModule {}
