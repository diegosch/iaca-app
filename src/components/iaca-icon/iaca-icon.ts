import { Component, Input } from '@angular/core';

/**
 * Component para íconos inline SVG (para reducir código)
 */
@Component({
  selector: 'iaca-icon',
  templateUrl: 'iaca-icon.html'
})
export class IACAIconComponent {

  symbols = 'assets/icons-defs.svg'
  @Input() name: string;

  get src_icon() {
    return this.symbols+'#icon-'+this.name;
  }
}
