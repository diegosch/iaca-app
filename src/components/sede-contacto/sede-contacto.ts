import { Component, Input } from '@angular/core';
import { InfoSede } from '../../pages/sede/info-sede';

/**
 * Datos de contacto de usa sede
 *  Usado en page sede y contacto
 */
@Component({
  selector: 'sede-contacto',
  templateUrl: 'sede-contacto.html'
})
export class SedeContactoComponent {

  @Input() sede: InfoSede;

  constructor() { }

}
