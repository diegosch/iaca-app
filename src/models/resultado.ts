import parse from 'date-fns/parse'; // https://date-fns.org/v2.0.0-alpha.11/docs/parse

/** Datos de un resultado obtenidos del request (exame)  */
export interface ResultadoAttributes {
  osNumero: string,
  data: string, // fecha dd/mm/yyyy
  hora: string, // hh:mm:ss
  nomeExames: string, // nombre de los análisis
  urlPdf?: string // link descarga, si está disponible
};

/** Model Resultado: representación de un resultado de un paciente */
export class Resultado {
  nro: string;
  fecha: Date; // fecha y hora
  nombre: string;
  url_pdf: string;

  constructor(attr?: ResultadoAttributes, data: any = null) {

    if (attr) {
      // Resultado creado desde data obtenida de WS
      this.nro = attr.osNumero;
      this.nombre = attr.nomeExames

      // parse fecha
      let fecha_hora = attr.data+' '+attr.hora;
      this.fecha = parse(fecha_hora,'dd/MM/yyyy HH:mm:ss', new Date());

      if (attr.urlPdf) {
        this.url_pdf = attr.urlPdf;
      }
    }
    else if (data) {
      // Resultado creado con data de storage
      Object.assign(this,data);
    }
  }

  // Estado del análsis: si no tiene URL PDF se asume en proceso
  get estado() {
    return (this.url_pdf) ? 'Completo' : 'En proceso';
  }
  get estado_class() {
    return (this.url_pdf) ? 'completado' : 'en-proceso';
  }

}