import { UserAttributes } from './user';
import { Resultado } from './resultado';

// Datos extra, obtenidos en get resultados (atributos recibidos en WS)
export interface UserAttributes {
  codigo?: string,
  nome?: string,
  endereco?: string,
  telefone?: string
};

/** Model User: representación de usuario logueado en la app */
export class User {
  tipo: string; // tipo de usuario (sólo soportado P: paciente)
  username: string; // usado para login y obtener resultados
  password: string; // usado para login y obtener resultados

  // Datos extra, obtenidos en get resultados
  codigo: string; // atributo en WS: codigo (ID del paciente)
  nombre: string; // atributo en WS: nome
  direccion: string; // atributo en WS: endereco
  telefono: string; // atributo en WS: telefone

  last_update: Date; // Última fecha/hora en la que se obtuvo lista de resultados

  resultados: Resultado[];

  constructor(data?: any) {
    if (data) {
      Object.assign(this,data);
    }
  }

  setDatosLogin(username,password,tipo) {
    this.tipo = tipo;
    this.username = username;
    this.password = password;
  }

  setAtributos(attr: UserAttributes) {
    if (attr.codigo) {
      this.codigo = attr.codigo;
    }
    if (attr.nome) {
      this.nombre = attr.nome;
    }
    if (attr.endereco) {
      this.direccion = attr.endereco;
    }
    if (attr.telefone) {
      this.telefono = attr.telefone;
    }
  }

  getNombre() {
    return (this.nombre) ? this.nombre : 'Paciente';
  }

}