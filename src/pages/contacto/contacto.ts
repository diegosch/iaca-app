import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { ConnectivityService } from '../../providers/connectivity.service';
import { InfoSede } from '../sede/info-sede';
import { HttpClient } from '@angular/common/http';

/**
 * Page contacto: info general.
 *  Link a whatsapp
 *  Datos de contacto de sedes obtenida de assets/data/sedes.json
 */
@IonicPage()
@Component({
  selector: 'page-contacto',
  templateUrl: 'contacto.html',
})
export class ContactoPage {

  sedes_list: InfoSede[] = [];

  constructor(public connectivity: ConnectivityService, private http: HttpClient) { }

  ionViewDidLoad() {
    this.getInfoSedes();
  }

  getInfoSedes() {
    if (this.sedes_list.length) {
      return;
    }
    this.http.get<InfoSede[]>('assets/data/sedes.json')
      .subscribe(
        data => { this.sedes_list = data; },
        error => { console.error(error); }
      );
  }

}
