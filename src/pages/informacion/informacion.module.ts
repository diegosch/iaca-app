import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InformacionPage } from './informacion';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    InformacionPage,
  ],
  imports: [
    IonicPageModule.forChild(InformacionPage),
    ComponentsModule
  ],
})
export class InformacionPageModule {}
