import { Component } from '@angular/core';
import { IonicPage, Platform } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import _ from 'lodash';

/**
 * Page Información: links a web
 */
@IonicPage()
@Component({
  selector: 'page-informacion',
  templateUrl: 'informacion.html',
})
export class InformacionPage {

  constructor(private browser: InAppBrowser, private platform: Platform) {  }

  openLink(url) {
    // Abre página con inappbrowser si está en device

    let target = '_blank';
    if (this.platform.is('ios')) {
      target = '_system';
    }
    let browser_link = this.browser.create(url,target);
    if (_.isEmpty(browser_link)) {
      // Si es vacío no está en device (web app): no usa inappbrowser, directamente abre con window.open
      return;
    }
    // Abro link en in-app-browser, e inserto css para ocultar header.
    browser_link.on('loadstop').subscribe(
      () => {
        // Luego de cargar la página inserta CSS para ocultar header
        browser_link.insertCSS(
          {code: ".menu-top-bg, .container header nav.menu-principal, .container header .breadcrumb-nav {display:none !important;}"}
        );
      }
    );
    // Si hay error al cargar la página, la cierra
    browser_link.on('loaderror').subscribe(()=> {
      browser_link.close();
    });
  }

}
