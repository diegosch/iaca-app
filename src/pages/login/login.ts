import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController,
         AlertController, LoadingController } from 'ionic-angular';
import { AuthService } from '../../providers/auth.service';
import { PAGES } from '../../app/config';
import { WSError, ERROR_CODES } from '../../providers/shiftWS.service';

/**
 * Page login: mostrada inicialmente si no hay usuario logueada, o accesible desde "cambiar usuario"
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  username: string = '';
  password: string = '';

  constructor(private auth: AuthService, private navCtrl: NavController,
              private toastCtrl: ToastController, private alertCtrl: AlertController,
              private loadingCtrl: LoadingController) {
  }

  login() {
    if (!this.username.trim() || !this.password.trim()) {
      this.toastCtrl.create({
        message: 'Debés indicar tu usuario y contraseña.',
        duration: 4000,
        position: 'bottom'
      }).present();
      return;
    }
    let loading = this.loadingCtrl.create();
    loading.present();
    this.auth.login(this.username,this.password)
      .then(() => {
        // Login OK: redirecciona a resultados
        this.navCtrl.setRoot(PAGES.resultados);
      })
      .catch((error: WSError) => {
        let msj_error;
        switch(error.codigo) {
          case ERROR_CODES.auth_error:
            msj_error = 'Usuario y/o contraseña inválidos.';
            break;
          case ERROR_CODES.user_invalid_tipo:
            msj_error = 'Tipo de usuario inválido. Esta aplicación es para pacientes del laboratorio.'
            break;
          default:
            msj_error = 'No se pudo iniciar sesión. Intentá de nuevo más tarde';
        }
        this.toastCtrl.create({
          message: msj_error,
          duration: 3000,
          position: 'bottom'
        }).present();
      })
      .then(() => loading.dismiss());
  }

  infoPacientes() {
    this.navCtrl.setRoot(PAGES.informacion);
  }

  showAclaracionDatosLogin() {
    let alert = this.alertCtrl.create({
      title: 'Instrucciones de acceso',
      message: `
        El usuario y contraseña se informan junto con el comprobante del análisis que entrega el laboratorio.<br>
        <b>Usuario</b>: Es un código formado por la letra P y una secuencia de números. Ejemplo: P2473.<br>
        <b>Contraseña</b>: Es generada con letras mayúsculas y números.<br><br>
        Esta aplicación está destinada para consulta de resultados de análisis realizados por pacientes de IACA Laboratorios.
        No está habilitada para consultas de derivaciones (acceso de profesionales).
        `,
      buttons: ['Entiendo']
    });
    alert.present();
  }

}
