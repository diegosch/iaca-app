import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from '../../components/components.module';
import { ResultadosPage } from './resultados';
import { FormatFechaPipe } from '../../pipes/format-fecha';

@NgModule({
  declarations: [
    ResultadosPage,
    FormatFechaPipe
  ],
  imports: [
    IonicPageModule.forChild(ResultadosPage),
    ComponentsModule
  ],
})
export class ResultadosPageModule {}
