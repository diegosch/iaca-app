import { Component } from '@angular/core';
import { IonicPage, ToastController, NavController, PopoverController, Refresher, InfiniteScroll } from 'ionic-angular';
import { AuthService } from '../../providers/auth.service';
import { ResultadosService } from '../../providers/resultados.service';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { WSError } from '../../providers/shiftWS.service';
import { Resultado } from '../../models/resultado';
import { PopoverPaciente } from '../../app/app.component';
import { PAGES } from '../../app/config';
import { trigger, state, style, animate, transition } from '@angular/animations';

/**
 * Página con listado de resultados de análisis del usuario logueado.
 *  Esta página se direcciona luego de hacer login,
 *  o funciona coomo página de inicio en caso de que ya se hizo login en ejecución previa de la app.
 *  Cuando se abre por primera vez, consulta resultados actualizados
 */
@IonicPage()
@Component({
  selector: 'page-resultados',
  templateUrl: 'resultados.html',
  animations: [
    // Animación spinner de loading inicial
    trigger('loadingInicial', [
      state('loading', style({
        height: '80px',
        'border-bottom-width': '1px'
      })),
      state('hidden', style({
        height: 0,
        'border-bottom-width': 0
      })),
      transition('loading => hidden', [
        animate('0.3s ease-out')
      ])
    ])
  ]
})
export class ResultadosPage {

  loading = false;
  resultados: Resultado[] = [];
  empty: boolean = false;
  cant_mostrados = 10;
  // Lista de resultados mostrados: inicialmente 10 para optimizar el scroll.
  //    Con IonInfiniteScroll se van agregando de a 10.
  get resultados_mostrados() {
    if (this.cant_mostrados > this.resultados.length) {
      return this.resultados;
    }
    return this.resultados.slice(0,this.cant_mostrados);
  }

  constructor(public auth: AuthService, private resultadosService: ResultadosService, private navCtrl: NavController,
              private loadingCtrl: LoadingController, private toastCtrl: ToastController, private popoverCtrl: PopoverController) {
  }

  ionViewCanEnter() {
    return this.auth.logueado;
  }

  ionViewWillEnter() {
    if (!this.auth.logueado) {
      this.navCtrl.setRoot(PAGES.login);
    }
  }

  ionViewDidLoad() {
    // En la primera carga de esta page con el usuario en esta ejecución de la app, intenta obtener data de storage
    if (!this.resultadosService.inicializado) {
      this.resultadosService.getResultadosStorage()
        .then(resultados => this.resultados = resultados)
        .catch(() => {
          // console.log('No hay resultados en storage')
        })
        // Luego actualiza pidiendo nuevos al WebService
        .then(() => {
          this.getResultados(true);
        });
    }
    else {
      // Obtiene los resultados: si es la primera ejecución, los obtiene del WS, si no, de lo guardado en memoria
      this.getResultados();
    }
  }

  // Obtiene resultados guardados en memoria o haciendo request al WS.
  // Si force_update es true, directamente hace request al WS
  // Si refresher esta definido, se esta actualizando desde el ion-refresher, si no, usa loading
  getResultados(force_update = false, refresher?: Refresher) {
    if (!refresher) {
      this.loading = true;
    }
    try {
      this.resultadosService.getResultados(force_update)
        .then(resultados => {
          this.resultados = resultados;
          this.empty = resultados.length == 0;
        })
        .catch((error: WSError) => {
          console.error(error);
          this.toastCtrl.create({
            message: error.descripcion,
            duration: 3000,
            position: 'bottom'
          }).present();
        })
        .then(() => {
          (refresher) ? refresher.complete() : this.loading = false;
        });
    }
    catch (error) {
      console.error(error);
      (refresher) ? refresher.complete() : this.loading = false;
    }
  }

  verMasResultados(infiniteScroll: InfiniteScroll) {
    this.cant_mostrados += 10;
    infiniteScroll.complete();
  }

  downloadPDF(resultado: Resultado) {
    if (!resultado.url_pdf) {
      return; // No hace nada
    }
    let loading = this.loadingCtrl.create();
    loading.present();
    this.resultadosService.downloadPDF(resultado)
      .then(() => loading.dismiss())
      .catch(() => {
        loading.dismiss();
        this.toastCtrl.create({
          message: `No se pudo descargar el PDF.`,
          position: 'middle',
          showCloseButton: true,
          closeButtonText: 'ACEPTAR',
          cssClass: 'toast-error-descarga-pdf'
        }).present();
      });
  }

  showPopoverPaciente(event) {
    let popover = this.popoverCtrl.create(PopoverPaciente,{},{cssClass:'popover-paciente-header'});
    popover.present({ev: event});
  }

}
