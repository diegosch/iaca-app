export interface InfoSede {
  nombre: string,
  nombre_lista: string,
  color: string, // clase
  color_fondo: string, // color HTML
  thumbnail: string,
  slides: string[],
  texto: string,
  direccion: string,
  localidad: string,
  coords: {
    lat: number,
    lng: number
  },
  telefono: {
    call: string,
    display: string
  },
  horarios?: {
    semana: string,
    sabado?: string
  },
  email?: string
};