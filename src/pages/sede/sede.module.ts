import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SedePage } from './sede';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SedePage,
  ],
  imports: [
    IonicPageModule.forChild(SedePage),
    ComponentsModule
  ],
})
export class SedePageModule {}
