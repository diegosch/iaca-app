import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavParams, Content } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { GoogleMaps, GoogleMap, GoogleMapOptions, GoogleMapsEvent, CameraPosition, ILatLng } from '@ionic-native/google-maps';
import { InfoSede } from './info-sede';
import _ from 'lodash';

/**
 * Página sede: info de una sede.
 *  Obtiene la data de assets/data/sedes.json, a partir del index dentro del arreglo
 *
 * Lógica de google maps basada en https://www.joshmorony.com/creating-an-advanced-google-maps-component-in-ionic-2/
 */
@IonicPage()
@Component({
  selector: 'page-sede',
  templateUrl: 'sede.html',
})
export class SedePage {

  index_sede: number;
  sede: InfoSede;
  show_map = false;
  loading_map = false;

  @ViewChild(Content) content: Content;
  @ViewChild('map') mapElement: ElementRef;
  map: GoogleMap = null;

  constructor(public navParams: NavParams, private http: HttpClient) {
    this.index_sede = navParams.get('index_sede')
  }

  ionViewDidLoad() {
    this.getInfoSede(this.index_sede); // Obtiene data de la sede y muestra mapa
  }

  getInfoSede(index) {
    this.http.get<InfoSede[]>('assets/data/sedes.json')
      .subscribe(
        data => {
          this.sede = data[index];
          if (this.sede && this.sede.coords)
            this.showMap();
        },
        error => { console.error(error); }
      );
  }

  showMap() {
    this.show_map = true;
    if (!this.map) { // La primera vez debe cargar el mapa. Espera 1 segundo
      this.loading_map = true;
      setTimeout(()=> {
        this.loadGoogleMap();
      },1000)
    }
  }

  loadGoogleMap() {
    // create CameraPosition
    let mapPosition: CameraPosition<ILatLng> = {
      target: {
        lat: this.sede.coords.lat,
        lng: this.sede.coords.lng
      },
      zoom: 14
    };
    let mapOptions: GoogleMapOptions = {
      camera: mapPosition,
      mapType: 'MAP_TYPE_ROADMAP'
    };
    this.map = GoogleMaps.create(this.mapElement.nativeElement,mapOptions);

    if (_.isEmpty(this.map)) {
      // Si no puede crear el mapa (por ej, no está en dispositivo) retorna
      this.show_map = false;
      this.loading_map = false;
      return;
    }

    // Wait the MAP_READY before using any methods.
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        this.map.addMarker({
          title: this.sede.nombre,
          position: {
            lat: this.sede.coords.lat,
            lng: this.sede.coords.lng
          }
        });
        this.loading_map = false;
      });
  }

}
