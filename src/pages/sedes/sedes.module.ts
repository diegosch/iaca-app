import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SedesPage } from './sedes';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SedesPage,
  ],
  imports: [
    IonicPageModule.forChild(SedesPage),
    ComponentsModule
  ],
})
export class SedesPageModule {}
