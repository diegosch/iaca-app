import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { InfoSede } from '../sede/info-sede';
import { PAGES } from '../../app/config';

/**
 * Page con lista de sedes (laboratorios) y acceso a cada uno.
 *  Datos obtenidos de assets/data/sedes.json
 */
@IonicPage()
@Component({
  selector: 'page-sedes',
  templateUrl: 'sedes.html',
})
export class SedesPage {

  sedes_list: InfoSede[] = [];

  // Dependencia http para obtener data de json local
  constructor(private http: HttpClient, private navCtrl: NavController) { }

  ionViewDidLoad() {
    this.getInfoSedes();
  }

  getInfoSedes() {
    if (this.sedes_list.length) {
      return;
    }
    this.http.get<InfoSede[]>('assets/data/sedes.json')
      .subscribe(
        data => { this.sedes_list = data; },
        error => { console.error(error); }
      );
  }

  showSede(index: number) {
    this.navCtrl.push(PAGES.sede,{ index_sede: index });
  }
}
