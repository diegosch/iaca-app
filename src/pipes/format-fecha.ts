import { Pipe, PipeTransform } from '@angular/core';
import format from 'date-fns/format'; // https://date-fns.org/docs/format

@Pipe({
  name: 'formatFecha'
})
export class FormatFechaPipe implements PipeTransform {

  transform(date: Date | number, formato?: string): string {
    if (!date) {
      return '-';
    }
    return format(date,formato);
  }

}
