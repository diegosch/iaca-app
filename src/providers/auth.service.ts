import { Platform, Events } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Device } from '@ionic-native/device';
import { ShiftWSService, WSError, ERROR_CODES } from './shiftWS.service';
import { WS_METHODS } from '../app/config';
import { User, UserAttributes } from '../models/user';

/* Nombre usados como index en storage local */
const STORAGE_KEY_USER = 'user';

/**
 * AuthService provider: encargado de autenticación de usuario, desde el service,
 *   o por haber guardado user y pass en localstorage
 */
@Injectable()
export class AuthService {

  inicializado: boolean; // Flag que indica si ya se hizo chequeo de login previo
  waiting: boolean;
  user: User;
  get logueado(): boolean {
    return this.user !== null;
  }

  constructor(private shiftWS: ShiftWSService, private storage: Storage,
              private platform: Platform, private device: Device, private events: Events) {
    this.user = null;
  }

  /**
   * Chequea si hubo login en uso previo de la app, obteniendo de local storage la información guardada del usuario
   *  Retorna boolean indicando si hay usuario logueado
   */
  checkLoginPrevio(): Promise<boolean> {
    this.waiting = true;
    this.user = null;
    // console.log('checkLoginPrevio');
    // Intento obtener data de usuario de local storage
    return this.storage.get(STORAGE_KEY_USER)
      .then(user_data => {
        if (user_data) {
          // Había usuario logueado: creo objeto
          this.user = new User(user_data);
        }
        // console.log('User en storage: ',this.user);
        this.waiting = false;
      })
      .catch(() => this.waiting = false ) // No debería ocurrir error
      .then(() => {
        this.inicializado = true;
        return this.user ? true : false; // Retorna promise con boolean indicando si hay usuario
      });
  }

  /**
   * Realiza login: hace get al web service con user y pass, y si la respuesta es exitosa, crea el model User
   *  Si falla, retorna error
   */
  login(username: string, password: string): Promise<User> {
    this.waiting = true;

    let params = new Map().set('pUserId',username).set('pSenha',password);

    // Parámetros opcionales: plataforma y device id
    if (this.platform.is('android')) {
      params.set('pPlataforma','android');
    }
    else if (this.platform.is('ios')) {
      params.set('pPlataforma','ios');
    }
    if (this.device && this.device.uuid) {
      params.set('pTokenPN',this.device.uuid);
    }

    return this.shiftWS.requestWS(WS_METHODS.login,params)
      .catch(error => {
        // console.error('Error login:',error);
        this.waiting = false;
        // La respuesta es inválida
        throw error;
      })
      .then(result => {
        // console.log('result login',result);
        if (!result.exito) {
          throw new WSError(ERROR_CODES.auth_error,result.error_msj);
        }
        if (result.data.tipo != 'P') {
          throw new WSError(ERROR_CODES.user_invalid_tipo,'El usuario no es de tipo paciente');
        }
        this.user = new User();
        this.user.setDatosLogin(username,password,result.data.tipo)
        // console.log(this.user);
        // Seteo usuario en storage local
        this.storage.set(STORAGE_KEY_USER,this.user);
        // Lanzo evento que reseta lista de resultados si había
        this.events.publish('auth:login');
        this.waiting = false;
        return this.user;
      });
  }

  /** Actualiza los datos de usuario, recibidos en get de resultados */
  updateUser(datos: UserAttributes) {
    this.user.setAtributos(datos);
    this.storage.set(STORAGE_KEY_USER,this.user);
  }

  logout() {
    this.user = null;
    // Limpio local storage
    this.storage.clear();
    // Lanzo evento para reset resultados y redireccion a Login
    this.events.publish('auth:logout');
  }

}
