import { AppAvailability } from '@ionic-native/app-availability';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Network } from '@ionic-native/network';

declare var Connection;

/**
 * Provider para chequear estado de conexión y disponibilidad de aplicaciones
 * Chequeo de estado de conexión basado en:
 *   https://www.joshmorony.com/creating-an-advanced-google-maps-component-in-ionic-2/
 *
 */
@Injectable()
export class ConnectivityService {

  onDevice: boolean;

  constructor(private platform: Platform, private network: Network, private appAvailability: AppAvailability){
    this.onDevice = this.platform.is('cordova');
  }

  isOnline(): boolean {
    if(this.onDevice && this.network.type){
      return this.network.type !== Connection.NONE;
    }
    else {
      return navigator.onLine;
    }
  }

  isOffline(): boolean {
    return !this.isOnline();
  }


  // Chequea si hay app disponible y abre ahí el link en la app, si no usa el link web
  //    Usado para redes sociales
  private checkAppOpenLink(app, app_link, web_link): Promise<void> {
    if (!app) {
      window.open(web_link,'_system');
      return Promise.resolve();
    }

    return this.appAvailability.check(app)
      .then(
        (yes) => window.open(app_link,'_system'),
        (no) => window.open(web_link,'_system')
      )
      .catch(() => window.open(web_link,'_system'))
      .then(() => { return Promise.resolve() }) ;
  }

  openFacebook(): Promise<void> {
    let app = null;
    if (this.platform.is('ios')) {
      app = 'fb://';
    } else if (this.platform.is('android')) {
      app = 'com.facebook.katana';
    }

    return this.checkAppOpenLink(app,
      'fb://page/426705210757924',
      'https://m.facebook.com/426705210757924');
  }

  openInstagram(): Promise<void> {
    let app = null;
    if (this.platform.is('ios')) {
      app = 'instagram://';
    } else if (this.platform.is('android')) {
      app = 'com.instagram.android';
    }

    return this.checkAppOpenLink(app,
      'instagram://user?username=iacalaboratorios',
      'https://www.instagram.com/iacalaboratorios');
  }

  openWhatsApp(): Promise<void> {

    let app = null;
    let telefono = 5492914320418;
    if (this.platform.is('ios')) {
      app = 'whatsapp://';
    } else if (this.platform.is('android')) {
      app = 'com.whatsapp';
    }

    return this.checkAppOpenLink(app,
      'whatsapp://send?phone='+telefono,
      'https://api.whatsapp.com/send?phone='+telefono);
  }

}
