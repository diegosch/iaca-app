import { Injectable } from '@angular/core';
import { Events, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ShiftWSService, WSError, ERROR_CODES, ERROR_MSJS } from './shiftWS.service';
import { AuthService } from './auth.service';
import { ConnectivityService } from './connectivity.service';
import { ResultadoAttributes, Resultado } from '../models/resultado';
import { WS_METHODS } from '../app/config';
import _ from 'lodash';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { FileOpener } from '@ionic-native/file-opener';

const STATUS_NO_REQUEST = 'no_request'; // aún no se hizo request al WS con el usuario actual
const STATUS_OK = 'ok'; // ya se hizo request al WS y respondió exitosamente (se procesaron los resultados si hay)
const STATUS_ERROR = 'error'; // ya se hizo request al WS y respondió con error
const STATUS_WAITING = 'waiting'; // Hizo request al WS y está esperando respuesta

/* Nombre usados como index en storage local */
const STORAGE_KEY_RESULTADOS = 'resultados';


/**
 * ResultadosService: encargado de obtener y procesar resultados de análisis del web service de Shift o de localstorage
 */
@Injectable()
export class ResultadosService {

  status_request: string = STATUS_NO_REQUEST;
  resultados: Resultado[] = [];

  get inicializado(): boolean  {
    return this.status_request != STATUS_NO_REQUEST && this.status_request != STATUS_ERROR;
  }

  constructor(private auth: AuthService, private shiftWS: ShiftWSService, private connectivity: ConnectivityService,
              private storage: Storage, private events: Events, private platform: Platform,
              private file: File, private fileTransfer: FileTransfer, private fileOpener: FileOpener) {
    this.resetResultados();
    // Listen evento login y logout, para resetear lista de resultados
    this.events.subscribe('auth:logout',() => {
      // console.log('Logout: resetResultados');
      this.resetResultados(); // No es necesario clean storage porque ya lo hace logout
    });
    this.events.subscribe('auth:login',() => {
      // console.log('Login: resetResultados');
      this.resetResultados(true);
    });
  }

  /**
   * Obtiene el listado de resultados:
   *  Por defecto retorna los resultados que están en memoria (por get previo),
   *    o hace request al WS si se invoca el get por primera vez o si no hay resultados.
   * Se puede pedir que haga sí o sí el request (force_update = true).
   * Si el segundo parametro es true, sólo obtiene los resultados guardados en usos previos de la app (local storage)
   * Si obtiene la data del WS, actualiza los datos del usuario y
   *    guarda esos datos y los resultados en local storage (cache para proximas ejecuciones)
   * Si falla el request por error de autenticación, hace logout.
   *
   * @param force_update boolean
   */
  getResultados(force_update = false): Promise<Resultado[]> {

    if (!this.auth.user) {
      throw new WSError(ERROR_CODES.auth_error,'Error getResultados: no hay usuario logueado');
    }

    if (force_update) {
      // console.log('getResultados de WS...');
      return this.requestResultadosWS();
    }
    if (this.status_request == STATUS_OK && this.resultados && this.resultados.length) {
      // console.log('getResultados guardado en memoria');
      return Promise.resolve(this.resultados);
    }
    else {
      // con status error o no inicializado, hace request al WS
      // console.log('getResultados de WS...');
      return this.requestResultadosWS();
    }
  }

  /**
   * Realiza request al WebService para obtener la lista actualizada de resultados.
   *  Actualiza datos del usuario paciente.
   *  Si falla retorna error con mensaje para usuario. Si es por error de autenticación, hace logout
   */
  private requestResultadosWS(): Promise<Resultado[]> {
    this.status_request = STATUS_WAITING;
    let params = new Map().set('pUserId',this.auth.user.username).set('pSenha',this.auth.user.password);
    return this.shiftWS.requestWS(WS_METHODS.resultados,params)
      .catch((error: WSError) => {
        console.error('Error request resultados WS', error);
        this.status_request = STATUS_ERROR;
        throw new WSError(ERROR_CODES.resultados_error,'No se pudo obtener del servidor la lista de resultados de análisis del paciente actualizada.');
      })
      .then(result => {
        // Si se deslogueó mientras esperaba el resultado, lanza excepción
        if (!this.auth.logueado) {
          console.error('Error request resultados: no hay usuario');
          this.status_request = STATUS_ERROR;
          throw new WSError(ERROR_CODES.resultados_error,'Usuario deslogueado, no pueden consultarse resultados.');
        }
        // Error en GET, o no incluye data de paciente esperada
        if (!result.exito || !result.data.paciente) {
          this.status_request = STATUS_ERROR;
          if (result.error_msj == ERROR_MSJS.auth_error) {
            console.error('Error request resultados: error auth');
            // Error auth: desloguea. Con events de ionic, se cambia a page login.
            this.auth.logout();
            throw new WSError(ERROR_CODES.auth_error,'No se pudieron consultar los resultados. Vuelva a iniciar sesión.');
          }
          throw new WSError(ERROR_CODES.resultados_error,'No se pudo obtener del servidor la lista de resultados de análisis del paciente actualizada.');
        }
        // Procesa datos recibidos, generando objetos Resultado. También actualiza datos de usuario
        //  Retorna los resultados, que se guarda en variable del service para próximos gets (reemplaza los que había antes)
        this.resultados = this.procesarResultados(result.data.paciente);

        // Guardar en localstorage los nuevos
        this.guardarResultadosStorage(this.resultados);
        // console.log('response WS resultados: ', this.resultados);
        this.status_request = STATUS_OK;
        return this.resultados;
      });
  }

  private procesarResultados(data_paciente: any): Resultado[] {

    // Actualiza datos recibidos del usuario (nombre, código, dirección, telefono) y guarda en storage
    this.auth.updateUser(_.omit(data_paciente,['exames']));

    if (!data_paciente.exames) {
      this.status_request = STATUS_ERROR;
      console.error('Error request resultados: no hay examenes');
      throw new WSError(ERROR_CODES.resultados_error,'La lista de resultados consultada al servidor es inválida. Volvé a intentar más tarde.');
    }

    if ( !data_paciente.exames.exame) {
      // Resultado vacío
      return [];
    }
    // Al convertir el XML recibido en objeto JSON, el arreglo de objetos exame dentro de exames, se convierte en exames.exame: [arreglo] (salvo que haya un solo examen)
    if (!_.isArray(data_paciente.exames.exame)) {
      // Si hay un solo resultado, no retorna arreglo el arse del XML.
      data_paciente.exames.exame = [data_paciente.exames.exame];
    }

    let resultados: Resultado[] = [];

    data_paciente.exames.exame.forEach((data_resultado: ResultadoAttributes) => {
      resultados.push(new Resultado(data_resultado));
    });

    return resultados;
  }

  /**
   * Guarda en storage los resultados obtenidos
   *  Por el momento se guarda directamente la lista de resultados, reemplazando lo que había previamente
   *  ToDo: sólo guardar las nuevas o reemplazar las modificadas, para llevar registro de las leídas y guardar link de PDF
   */
  private guardarResultadosStorage(resultados: Resultado[]) {
    // ToDo recorrer arreglo previamente guardado y comparar con arreglo recibido, guardando sólos los nuevos o modificados
    // El arreglo se codifica en string
    this.storage.set(STORAGE_KEY_RESULTADOS,resultados);
  }

  /**
   * Obtiene los resultados almacenados en local storage por requests de usos anteriores de la app con el usuario logueado
   *  Por el momento sólo sirve en caso de visualización offline
   */
  getResultadosStorage(): Promise<Resultado[]> {
    return this.storage.get(STORAGE_KEY_RESULTADOS)
      .then(data_resultados => {
        if (data_resultados) {
          this.resultados = [];
          // Creo objetos (los atributos ya corresponden a los valores guardados)
          data_resultados.forEach(data_resultado => {
            this.resultados.push(new Resultado(null,data_resultado));
          });
          // console.log('Resultados en storage: ',this.resultados);
          return this.resultados;
        }
        else {
          throw 'No hay resultados guardados en storage';
        }
      });
  }

  /**
   * Descarga PDF.
   *  En web y iOS usa browser.
   *  En android, descarga, guarda en file system, y lo abre en aplicación
   *    Si está online descarga siempre.
   *    Si falla, o está offline, chequea si ya fue descargado para abrirlo
   *      (para esto, el filename se compone con nro orden y el principio del nombre del análisis,
   *       porque puede haber mas de un resultado con el mismo nro)
   *
   * Returna promise resolver indicando que fue descargado, o error si falla
   */
  downloadPDF(resultado: Resultado): Promise<void> {

    // console.log('Descarga PDF: '+resultado.url_pdf);

    /* Si no está en mobile, abre el PDF con el browser */
    if (!this.platform.is('cordova')) {
      window.open(resultado.url_pdf,'_system');
      return Promise.resolve();
    }
    /* iOS el browser muestra el PDF, no descarga */
    if (this.platform.is('ios')) {
      // ToDo: usar download de plugin http? https://ionicframework.com/docs/native/http/#downloadFile
      window.open(resultado.url_pdf,'_system','location=no');
      return Promise.resolve();
    }
    if (!this.platform.is('android')) {
      throw Error('Descarga no soportada por el dispositivo');
    }

    /* Android: descarga o abre archivo si está offline y lo descargó previamente  */
    return this.downloadPDFAndroid(resultado);

  }

  /**
   * Descarga en Android: intenta descargar y abrir, o chequear si había existencia previa
   * Retorna promise resolver indicando que fue descargado, o error si falla
   *
   * @param resultado
   */
  downloadPDFAndroid(resultado: Resultado): Promise<void> {

    if (this.connectivity.isOnline()) {
      return this.descargarAbrirPDFAndroid(resultado)
        .catch(() => {
          console.error('No puede descargar');
          // Si falló la descarga, chequea existencia previa del archivo para abrirlo
          return this.abrirPDFPreviamenteDescargadoAndroid(resultado);
        })
        .then(() => {
          // console.log('Archivo PDF abierto'); return;
        });
    }
    else {
      // Chequea existencia previa del archivo para abrirlo o falla
      return this.abrirPDFPreviamenteDescargadoAndroid(resultado)
        .then(() => {
          // console.log('Archivo PDF abierto'); return;
        });
    }
  }

  generarFilenameArchivo(resultado: Resultado): string {
    // Generación del nombre del archivo a descargar (o chequear descarga previa):
    //   usa el nro de orden y el comienzo del nombre, por si hay más de un resultado con el mismo nro.
    //   se forma siempre igual, permite chequeo posterior de existencia de archivo
    const nombres = (resultado.nombre) ? resultado.nombre.split(' ') : ['-'];
    const filename = 'IACA-Resultado-'+resultado.nro+'-'+nombres[0]+'.pdf';

    return filename;
  }

  /**
   * Consulta si el archivo ya fue descargado y lo abre.
   *  Si no estaba lanza error
   * @param resultado
   */
  abrirPDFPreviamenteDescargadoAndroid(resultado: Resultado): Promise<void> {
    const path = this.file.dataDirectory;
    const filename = this.generarFilenameArchivo(resultado);

    // console.log('Chequeo de archivo previamente descargado...');
    return this.file.checkFile(path,filename)
      // Si el archivo no está lanza error
      .catch(() => { return false })
      .then(existe => {
        if (existe) {
          // console.log('Archivo previamente descargado',path,filename);
          return this.abrirPDFAndroid(path + filename, resultado);
        }
        else {
          // console.log('El archivo no existía previamente');
          throw Error('Archivo no pudo ser descargado, ni se descargó previamente');
        }
      });
  }

  /**
   * Descarga archivo del URL indicado en resultado y luego lo abre
   *  Si no puede descargar lanza error
   * @param resultado
   */
  descargarAbrirPDFAndroid(resultado: Resultado): Promise<void> {

    const filename = this.file.dataDirectory + this.generarFilenameArchivo(resultado);

    // console.log('Descarga del archivo al dispositivo...',resultado.url_pdf, filename);
    const file_transfer = this.fileTransfer.create();
    return file_transfer.download(resultado.url_pdf, filename)
      .catch(error => {
        console.error('Error descarga',error); throw error;
      })
      .then(entry => {
        let url_local = entry.toURL();
        // console.log('Archivo descargado',url_local);
        return this.abrirPDFAndroid(url_local, resultado);
      });
  }

  /**
   * Abre archivo, y si falla hace fallback abriendo en browser
   *  Retorna promise resolver
   * @param file_path
   */
  abrirPDFAndroid(file_path, resultado: Resultado): Promise<void> {
    // console.log('Abrir archivo...');
    return this.fileOpener.open(file_path, 'application/pdf')
      .catch(() => {
        // console.log('Fallback abrir archivo: abre en browser...');
        // Fallback: abre en browser (el archivo había podido ser descargado, pero no funciona el lector de PDF)
        window.open(resultado.url_pdf,'_system','location=no');
        return Promise.resolve(true);
      });
  }

  /**
   * Quita lista de resultados de memoria y de local storage
   *  Invocado cuando se desloguea usuario o loguea uno nuevo
   */
  resetResultados(clean_storage = false) {
    this.status_request = STATUS_NO_REQUEST; // vuelve a estado sin inicializar
    this.resultados = [];
    if (clean_storage) {
      // Quito resultados de storage
      this.storage.remove(STORAGE_KEY_RESULTADOS);
    }
  }

}
