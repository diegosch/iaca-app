import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import X2JS from 'x2js';
import _ from 'lodash';

import { WEBSERVICE_URL, WS_SOAPACTION_URL, WEBSERVICE_URL_PROXY } from '../app/config';

interface WSResult {
  exito: boolean; // en WS: sucesso, se convierte en boolean en parseResponse
  error_msj?: string; // en WS: observacao
  data?: any; // Datos de las respuestas
}

interface IWSError {
  codigo: number;
  descripcion?: string;
  detalles?: any;
}
export class WSError implements IWSError {
  constructor(public codigo: number, public descripcion?: string, public detalles?: any) {}
}
export const ERROR_CODES = {
  ws_request_error: 1,
  ws_result_invalid: 2,
  ws_result_error: 3,
  auth_params_invalid: 10,
  auth_error: 11,
  user_invalid: 12,
  user_invalid_tipo: 13,
  resultados_error: 20,
  pdf_error: 30
};
// Mensajes de error enviados por el WebService
export const ERROR_MSJS = {
  auth_error: '-93:Usuário ou senha inválidos.'
}


/**
 * ShiftWSService provider: encargado de requests al web service de Shift
 *  En lugar del cliente HTTP de Angular, usa https://ionicframework.com/docs/native/http/
 *      para evitar restricciones CORS
 *  Se mantiene request con cliente HTTP de Angular, para uso con browser
 */
@Injectable()
export class ShiftWSService {

  constructor(private http: HTTP, private platform: Platform, private http_angular: HttpClient) { }

  /**
   * Request genérico al Web Service (protocolo SOAP) al método indicado
   *  Retorna promise con el resultado si la respuesta es válida, o rejected si falla
   */
  requestWS(method: string, params: Map<string,string>): Promise<WSResult> {

    let body = this.generarRequestBody(method,params);

    // console.log('requestWS '+method,params,body);

    // El URL es el mismo para todos los métodos (a nivel HTTP es un POST, pero semánticamente es un GET)
    return this.postHTTP(method, body)
      .catch(resp => { // Error del request
        console.error('Error request WebService',resp);
        // Maneja error y relanza excepción
        if (resp.status && resp.error) {
          throw new WSError(resp.status,`Error HTTP response WebService: ${resp.message}`); // No expongo todo el error
        }
        else {
          throw new WSError(ERROR_CODES.ws_request_error);
        }
      })
      .then(resp => {
        // console.log('requestWS XML response: ',resp);
        let data = this.parseResponse(resp.data, method);
        // console.log('requestWS result',data);
        // No chequea si el resultado es exitoso o no, eso queda a consideración de quien invoca

        return data;
      });
  }

  // Realiza request usando plugin HTTP de IonicNative, o fallback cliente angular en browser
  private postHTTP(method: string, body: string): Promise<any> {

    let headers = this.generarHTTPHeaders(method);

    if (this.platform.is('cordova')) {
      this.http.setDataSerializer('utf8');
      // POST con plugin HTTP: https://ionicframework.com/docs/native/http/#HTTPResponse
      return this.http.post(WEBSERVICE_URL, body, headers);
    }
    else {
      // Uso con browser: usa cliente HTTP, y proxy que genera headers CORS
      return this.http_angular.post(WEBSERVICE_URL_PROXY, body,
        { headers: new HttpHeaders(headers), responseType: 'text' })
      .toPromise() // convierte observable a promise
      .catch(error => { // Error del request
        // Transformo error a formato de Plugin HTTP https://github.com/silkimen/cordova-plugin-advanced-http#failure
        if (error instanceof HttpErrorResponse) {
          throw {
            status: error.status,
            error: error.error
          }
        }
        throw error;
      })
      .then(resp => {
        // Transformo resp a formato de Plugin HTTP https://github.com/silkimen/cordova-plugin-advanced-http#success
        //  https://ionicframework.com/docs/native/http/#HTTPResponse
        return {
          data: resp
        }
      })
    };
  }

  private generarHTTPHeaders(method: string): any {

    const headers = {
      'SOAPAction': WS_SOAPACTION_URL+method,
      'Content-Type': 'text/xml; charset=utf-8'
    };
    return headers;
  };

  /**
   * Genera el XML con el body para un request, según metodo y parametros
   *
   * @param method nombre del método: definidos en WS_METHODS
   * @param params mapeo key->value de parámetros del método (no incluye token)
   */
  private generarRequestBody(method: string, params: Map<string, string>) {

    let xml_params: string = '';
    params.forEach((value: string, key: string) => {
      xml_params += `<www:${key}>${value}</www:${key}>`;
    });

    return `
      <x:Envelope xmlns:x="http://schemas.xmlsoap.org/soap/envelope/" xmlns:www="http://www.shift.com.br">
        <x:Header/>
        <x:Body>
          <www:${method}>
            ${xml_params}
          </www:${method}>
        </x:Body>
      </x:Envelope>`;
  }

  /**
   * Parsea el XML de la respuesta, controla que exista nodo con el resultado del método
   *  y convierte el resultado a objeto JSON
   *
   * @param resp
   * @param method
   */
  private parseResponse(resp: string, method: string): WSResult {

    // Parseo el string generando DOM document para manipular, y buscar nodo con resultado
    let xmlDoc = new DOMParser().parseFromString(resp, 'application/xml');
    let nodo_result = method+'Result';

    // El resultado del request se encuentra en elemento <NombreMetodo>Result (ej: WsLoginResult)
    let result_dom = xmlDoc.querySelector(nodo_result);
    if (!result_dom) {
      throw new WSError(ERROR_CODES.ws_result_invalid,`Respuesta SOAP inválida para el método ${method}`,'Response: '+resp);
    }

    // Conversión de DOM a JSON a partir del nodo <nombre_metodo>Result
    //    Librería usada: https://github.com/x2js/x2js
    let x2js = new X2JS();
    let result_obj = x2js.xml2js(result_dom.outerHTML)[nodo_result]; // Obtiene keys dentro del resultado

    delete result_obj._xmlns; // quita atributo xmlns que agrega el DOM parser al obtener outerHTML

    // Transformo objeto
    let result: WSResult = {
      exito: result_obj.sucesso == "1",
    };
    if (result_obj.observacao) {
      result.error_msj = result_obj.observacao;
    }
    else {
      // Datos de la respuesta
      result.data = _.omit(result_obj,['sucesso','observacao']);
    }

    return result;
  }

}
